# PT Loyal Member Indonesia

Test PT Loyal Member Indonesia
### Developer
* Regita Lisgiani

### Built With
* [Laravel](https://laravel.com/)

## Getting Started

First clone the project into your local machine

### Installation

1. Clone the project 
```sh
git clone https://gitlab.com/regitalisgianidrajat/ptloyalmovie
```
2. Whenever you clone a new Laravel project you must now install all of the project dependencies. This is what actually installs Laravel itself, among other necessary packages to get started.When we run composer, it checks the composer.json file which is submitted to the github repo and lists all of the composer (PHP) packages that your repo requires. Because these packages are constantly changing, the source code is generally not submitted to github, but instead we let composer handle these updates. So to install all this source code we run composer with the following command.
```sh
composer install
```
3. Create env (env that i use is already copied at .env.example file)  
   Dont forget to set the variable JWT_SECRET = LoyalIDAPI (in env)
4. create database and start run the terminal then type
 ```sh
php artisan make:database {database-name} {connection-name}
php artisan migrate
php artisan db:seed
```
5. Start to view, create, update, or delete by hit the public url (/public/movies)

### API

1. Import the collection into your postman [LoyalID Movie](https://www.getpostman.com/collections/4a97118f6ec7d245461e)
2. Make env for postman collection - endpoint url for this api example [URL](localhost/courses/TestMovie/public/api/)
```sh
//this is example env for the postman
{
	"id": "#",
	"name": "LoyalID",
	"values": [
		{
			"key": "TOKEN",
			"value": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjEsImlhdCI6MTU5OTg4NjUyNSwiZXhwIjoxNTk5OTcyOTI1LCJuYW1lIjoiTG95YWxJZCBEZXZlbG9wbWVudCIsInNlY3JldCI6ImxveWFsaWQyMDIwIiwidXNlciI6IkxveWFsSWQifQ.TmEBSO7Cl6KF4TN8Xpp7OQ-_9VZ2CbXHXXKiPigs_VY",
			"enabled": true
		},
		{
			"key": "URL",
			"value": "localhost/courses/TestMovie/public/api/",
			"enabled": true
		}
	]
}
```
3. To access the API you need to get credential token first by request token on postman > file API GET CREDENTIAL
```sh
//this is default auth for get the token
{
	user:LoyalId
	secret:loyalid2020
}

```
4. Set the token to the Authorization -> Barier Token


Note 
The laravel package that i use for this module is
1. JWT Firebase -> for security API using token and generate it using JWT
2. Unit testing -> to use this make sure you disabled the Middleware check token
3. Transformer -> i used this mostly for reponse in API
4. Faker and Seeder

