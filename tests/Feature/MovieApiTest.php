<?php

namespace Tests\Feature;
use Tests\TestCase;

class MovieApiTest extends TestCase
{
    public function testBasicTest()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    } 
    public function testShouldReturnAllMovies(){

        try{
            $response = $this->get("/api/movies");
            $response->assertStatus(200);
            $response->seeJsonStructure([
                'data' => ['*' =>
                    [
                        'movie_id',
                        'title',
                        'year',
                        'duration',
                        'language',
                        'release_date',
                        'country'
                    ]
                ]
            ]);
         }
         catch(\Exception $e){
            \Log::info($e->getMessage()); 
         }
        
        
    }


    
}
