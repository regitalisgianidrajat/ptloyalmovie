@extends('template.main')
@section('content')
<style>
.btn-light{
    padding:15px;
}
</style>
    <h1 class="mt-4 mb-4" style="margin-bottom:0px!important">{{$title}}</h1>
    
    <ol class="breadcrumb" style="background-color:#fff">
          <li class="breadcrumb-item"><a href="{{ url('/user/') }}">Home</a></li>
          <li class="breadcrumb-item active">{{ $title }}</li>
        </ol>
    <hr>
    @if($errors->any())
        <div class="card-body notif-message">
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-check"></i> Alert! </h5>
            {{$errors->first()}}
            </div>
        </div>
    @endif
    <form action="{{ url('/movies/update/'.$data['movie_id']) }}" method="post">
        {{csrf_field()}}
        @method('PUT')
        <div class="form-group">
            <label for="name">Title</label>
            <input class="form-control" type="text" name="title" id="title"  value="{{ $data['title'] }}" placeh$dataer="Snow White Princess" required>
        </div>
        <div class="form-group">
            <label for="name">Language</label>
            <input class="form-control" type="text" name="language" id="language" value="{{ $data['language'] }}" placeh$dataer="English" required>
        </div>
        <div class="form-group">
            <label for="name">Country</label>
            <input class="form-control" type="text" name="country" id="country" value="{{ $data['country'] }}" placeh$dataer="Singapore" required>
        </div>
        <div class="form-group">
            <label for="name">Duration</label>
            <input class="form-control" type="number" name="duration" id="duration" value="{{ $data['duration'] }}" placeh$dataer="10" required>
            <p><i>In Minutes</i><p>
        </div>
        <div class="form-group">
            <label for="name">Year Realese</label>
            <input class="form-control" type="year" name="year" id="year" value="{{ $data['year'] }}"  placeh$dataer="2019" required>
        </div>
        <div class="form-group">
            <label for="name">Realese Date</label>
            <input class="form-control" type="date" name="release_date" id="release_date" value="{{ $data['release_date'] }}"  placeh$dataer="10" required>
        </div>
        <div class="form-group">
            <label for="type">Directors</label>
            <select multiple name="directors[]" id="directors_id" class="form-control selectpicker" multiple data-live-search="true" style="height:50px!important">
                <option value="">--- Select Directors ---</option>
                @foreach ($directors as $key)
                <option
                        @foreach($data['director'] as $raw)
                            @if($raw['directors_id'] == $key['directors_id'])
                                {{'selected'}}
                            @endif
                        @endforeach
                    value="{{ $key['directors_id'] }}">{{  $key['director_name'] }}</option>
                @endforeach
            </select>
        </div> 
        <div class="form-group">
            <label for="type">Genres</label>
            <select multiple name="genres[]" id="genres_id" class="form-control selectpicker" multiple data-live-search="true" style="height:50px!important">
                <option value="">--- Select Genres ---</option>
                    @foreach ($genres as $key)
                    <option
                        @foreach($data['genre'] as $raw)
                            @if($raw['genres_id'] == $key['genres_id'])
                                {{'selected'}}
                            @endif
                        @endforeach
                        value="{{$key['genres_id']}}">{{$key['genre_title']}}</option>
                    @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="type">Actors</label>
            <select multiple name="actors[]" id="actors_id" class="form-control selectpicker" multiple data-live-search="true" style="height:50px!important">
                <option value="">--- Select Actors ---</option>
                @foreach ($actors as $key)
                    <option 
                    @foreach($data['actor'] as $raw)
                            @if($raw['actors_id'] == $key['actors_id'])
                                {{'selected'}}
                            @endif
                        @endforeach
                    value="{{ $key['actors_id'] }}">{{  $key['actors_name'] }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group float-right">
            <button class="btn btn-lg btn-danger" type="reset">Reset</button>
            <button class="btn btn-lg btn-primary" type="submit">Submit</button>
        </div>
        
    </form>

@endsection