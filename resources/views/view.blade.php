@extends('template.main')
@section('content')
    <h1 class="mt-4 mb-4">{{$title}}
    <a class="btn btn-primary float-right mt-2" href="{{url('/movies/add')}}" role="button">Create Movie</a></h2><hr>
    @if(Session::get('alert-success'))
        <div class="card-body notif-message">
            <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-check"></i> Alert! </h5>
            {{ Session::get('alert-success') }}
        </div>
        </div>
    @endif
    <table id="data_users_reguler" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Title</th>
                <th>Language</th>
                <th>Country</th>
                <th>Realese Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $row)
            <tr>
                    <td>{{ $row['title'] }}</td>
                    <td>{{ $row['language'] }}</td>
                    <td>{{ $row['country'] }}</td>
                    <td>{{ $row['release_date'] }}</td>
                    <td>
                        <a href="{{ url('/movies/edit/'.$row["movie_id"])}}" class="btn btn-xs btn-primary">Edit</a> |
                        <a href="{{ url('/movies/delete/'.$row["movie_id"])}}"" class="btn btn-xs btn-danger" onclick="return confirm('Are you want to delete this data ?');">Delete</a>
                    </td>
                </tr>
            @endforeach
            
    </table>
@endsection