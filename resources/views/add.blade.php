@extends('template.main')
@section('content')
<style>
.btn-light{
    padding:15px;
}
</style>
    <h1 class="mt-4 mb-4" style="margin-bottom:0px!important">{{$title}}</h1>
    
    <ol class="breadcrumb" style="background-color:#fff">
          <li class="breadcrumb-item"><a href="{{ url('/user/') }}">Home</a></li>
          <li class="breadcrumb-item active">{{ $title }}</li>
        </ol>
    <hr>
    @if($errors->any())
        <div class="card-body notif-message">
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-check"></i> Alert! </h5>
            {{$errors->first()}}
            </div>
        </div>
    @endif
    <form action="{{ url('/movies/save') }}" method="post">
        {{csrf_field()}}
        <div class="form-group">
            <label for="name">Title</label>
            <input class="form-control" type="text" name="title" id="title" value="{{ old('title') }}" placeholder="Snow White Princess" required>
        </div>
        <div class="form-group">
            <label for="name">Language</label>
            <input class="form-control" type="text" name="language" id="language" value="{{ old('language') }}" placeholder="English" required>
        </div>
        <div class="form-group">
            <label for="name">Country</label>
            <input class="form-control" type="text" name="country" id="country" value="{{ old('country') }}" placeholder="Singapore" required>
        </div>
        <div class="form-group">
            <label for="name">Duration</label>
            <input class="form-control" type="number" name="duration" id="duration" value="{{ old('duration') }}" placeholder="10" required>
            <p><i>In Minutes</i><p>
        </div>
        <div class="form-group">
            <label for="name">Year Realese</label>
            <input class="form-control" type="year" name="year" id="year" value="{{ old('year') }}"  placeholder="2019" required>
        </div>
        <div class="form-group">
            <label for="name">Realese Date</label>
            <input class="form-control" type="date" name="release_date" id="release_date" value="{{ old('release_date') }}"  placeholder="10" required>
        </div>
        <div class="form-group">
            <label for="type">Directors</label>
            <select multiple name="directors[]" id="directors_id" class="form-control selectpicker" multiple data-live-search="true" style="height:50px!important">
                <option value="">--- Select Directors ---</option>
                @foreach ($directors as $key)
                    <option value="{{ $key['directors_id'] }}">{{  $key['director_name'] }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="type">Genres</label>
            <select multiple name="genres[]" id="genres_id" class="form-control selectpicker" multiple data-live-search="true" style="height:50px!important">
                <option value="">--- Select Genres ---</option>
                @foreach ($genres as $key)
                    <option value="{{ $key['genres_id'] }}">{{  $key['genre_title'] }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="type">Actors</label>
            <select multiple name="actors[]" id="actors_id" class="form-control selectpicker" multiple data-live-search="true" style="height:50px!important">
                <option value="">--- Select Actors ---</option>
                @foreach ($actors as $key)
                    <option value="{{ $key['actors_id'] }}">{{  $key['actors_name'] }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group float-right">
            <button class="btn btn-lg btn-danger" type="reset">Reset</button>
            <button class="btn btn-lg btn-primary" type="submit">Submit</button>
        </div>
        
    </form>

@endsection