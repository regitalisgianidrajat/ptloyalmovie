<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use League\Fractal;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Transformers\MovieTransformer;
use Illuminate\Support\Facades\Validator;
use Firebase\JWT\JWT;

class Controller extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected function jwt($credential) {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $credential->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60*60*24,// Expiration time
            'name' => $credential->name,
            'secret' => $credential->secret,
            'user' => $credential->user,

        ];
        
        $response['TOKEN'] = JWT::encode($payload, env('JWT_SECRET'));
        // $response['token_credential'] = JWT::encode($payload, env('JWT_SECRET'), array('HS256'));
        return $this->ResponseStatus(200, 'Success! Token Generated !', $response);
    }
    protected function Validator(array $data, array $role, $first = true)
    {
        $validator = Validator::make($data, $role);
        if ($validator->fails()) {
            if (!$first) {
                $errors = $validator->getMessageBag()->first();

                foreach ($errors as $i=>$error) {
                    $errormsg[]['error'] = $error;
                }
                return $errormsg;
            }
            $errors = $validator->getMessageBag()->first();
            return $errors;
        }
    }
    protected function ResponseStatus($status_code, $message, $data)
    {
        return response()->json([
            'STATUS' => $status_code,
            'MESSAGE' => $message,
            'DATA' => $data
        ], $status_code);
    }
}
