<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Movies\MovieMasterModel;
use App\Models\Movies\MovieGenresModel;
use App\Models\Movies\MovieActorsModel;
use App\Models\Movies\MovieDirectorsModel;
use App\Models\Support\GenresModel;
use App\Models\Support\ActorsModel;
use App\Models\Support\DirectorsModel;
use League\Fractal;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Transformers\MovieTransformer;
use Redirect;

class MoviesController extends Controller
{
    private $fractal;
    private $movies;
    private $movie_genres;
    private $movie_actors;
    private $movie_directors;
    private $genres;
    private $actors;
    private $directors;

    public function __construct()
    {
        $this->fractal            = new Manager();  
        $this->movies             = MovieMasterModel::select('*');
        $this->movie_genres       = MovieGenresModel::select('*');
        $this->movie_actors       = MovieActorsModel::select('*');
        $this->movie_directors    = MovieDirectorsModel::select('*');
        $this->genres             = GenresModel::select('*');
        $this->actors             = ActorsModel::select('*');
        $this->directors          = DirectorsModel::select('*');
    }
    public function index(){
        $paginator      = MovieMasterModel::orderBy('movie_id','DESC')->paginate();
        $data           = $this->ResponseTransformer($paginator);
        $data['title']  = 'Manage Movie';
    	return view('view', $data);
    }

    public function create(){
        $data = $this->DataInput('Create Movie');
    	return view('add', $data);
    }
    public function store(Request $request)
    {
        $PostRequest = $request->only('title','language','country','duration','release_date','year');
        $role = [
            'title'         => 'Required',
            'language'      => 'Required',
            'country'       => 'Required',
            'duration'      => 'Required|min:0',
            'year'          => 'Required|min:0',
            'release_date'  => 'Required',
            "directors"     => "required|array|min:0",
            "genres"        => "required|array|min:0",
            "actors"        => "required|array|min:0"
        ];
        $ErrorMsg = $this->Validator($request->all(), $role);
        if (!empty($ErrorMsg)) {
            return Redirect::back()->withErrors([$ErrorMsg]);
        }
        $saved = MovieMasterModel::create($PostRequest);

        if(!$saved){
            return Redirect::back()->withErrors(['Server Error !']);
        }
        $Post['movie_id'] = $saved->id;
        for ($i=0; $i < count($request['actors']) ; $i++) { 
            $Post['actors_id'] = $request['actors'][$i];
            MovieActorsModel::create($Post);
        }
        for ($i=0; $i < count($request['directors']) ; $i++) { 
            $Post['directors_id'] = $request['directors'][$i];
            MovieDirectorsModel::create($Post);
        }
        for ($i=0; $i < count($request['genres']) ; $i++) { 
            $Post['genres_id'] = $request['genres'][$i];
            MovieGenresModel::create($Post);
        }
        
		return redirect('/movies')->with('alert-success','Movie Successfully Created !');
    }
    public function edit($id)
    {
        $data = $this->DataInput('Edit Movie');
        $data['data']     = $this->movies->with('genre')->with('director')->where('movie_id',$id)->first();
    	return view('edit', $data);
    }
    public function update(Request $request,$id)
    {
        $PostRequest = $request->only('title','language','country','duration','release_date','year');
        $role = [
            'title'         => 'Required',
            'language'      => 'Required',
            'country'       => 'Required',
            'duration'      => 'Required|min:0',
            'year'          => 'Required|min:0',
            'release_date'  => 'Required',
            "directors"     => "required|array|min:0",
            "genres"        => "required|array|min:0",
            "actors"        => "required|array|min:0"
        ];
        $ErrorMsg = $this->Validator($request->all(), $role);
        if (!empty($ErrorMsg)) {
            return Redirect::back()->withErrors([$ErrorMsg]);
        }
        $saved = MovieMasterModel::where('movie_id',$id)->update($PostRequest);

        if(!$saved){
            return Redirect::back()->withErrors(['Server Error !']);
        }
        MovieActorsModel::where('movie_id',$id)->delete();
        MovieDirectorsModel::where('movie_id',$id)->delete();
        MovieGenresModel::where('movie_id',$id)->delete();
        $Post['movie_id'] = $id;
        for ($i=0; $i < count($request['actors']) ; $i++) { 
            $Post['actors_id'] = $request['actors'][$i];
            MovieActorsModel::create($Post);
        }
        for ($i=0; $i < count($request['directors']) ; $i++) { 
            $Post['directors_id'] = $request['directors'][$i];
            MovieDirectorsModel::create($Post);
        }
        for ($i=0; $i < count($request['genres']) ; $i++) { 
            $Post['genres_id'] = $request['genres'][$i];
            MovieGenresModel::create($Post);
        }
        
		return redirect('/movies')->with('alert-success','Movie Successfully Created !');
    }
    public function destroy($id)
    {
        $saved = $this->movies->where('movie_id',$id)->delete();
        if(!$saved){
            return Redirect::back()->withErrors(['Server Error !']);
        }
        
		return redirect('/movies')->with('alert-success','Movie Successfully Deleted !');
        
    }


    public function ResponseTransformer($data){
        $return = $data->getCollection();
        $resource = new Collection($return, new MovieTransformer);
        $resource->setPaginator(new IlluminatePaginatorAdapter($data));
        
        return $this->fractal->createData($resource)->toArray();
    }
    public function DataInput($title){
        $data['actors']     = $this->actors->get();
        $data['genres']     = $this->genres->get();
        $data['directors']  = $this->directors->get();
        $data['title']      = $title;
        return $data;
    }

}
