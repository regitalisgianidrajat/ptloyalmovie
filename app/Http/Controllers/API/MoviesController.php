<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Movies\MovieMasterModel;
use App\Models\Movies\MovieGenresModel;
use App\Models\Movies\MovieActorsModel;
use App\Models\Movies\MovieDirectorsModel;
use App\Models\Support\GenresModel;
use App\Models\Support\ActorsModel;
use App\Models\Support\DirectorsModel;
use League\Fractal;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Transformers\MovieTransformer;
use App\Transformers\MovieTransformerDetail;

class MoviesController extends Controller
{
    private $fractal;

    public function __construct()
    {
        $this->fractal = new Manager();  
    }
    /**
     * GET /products
     * 
     * @return array
     */
    public function index(){
        $paginator = MovieMasterModel::paginate();
        if (empty($paginator)) {
            return $this->ResponseStatus('404', 'FAILED DATA NOT FOUND', array());
        }
        $movie = $paginator->getCollection();
        $resource = new Collection($movie, new MovieTransformer);
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
        
        return $this->ResponseStatus(200, 'Movie Data ', $this->fractal->createData($resource)->toArray()['data']);
    }
    public function show(Request $request){
        $PostRequest = $request->only(
            'movie_id'
        );

        $role = [
            'movie_id' => 'Required'
        ];
        $ErrorMsg = $this->Validator($PostRequest, $role);

        if (!empty($ErrorMsg)) {
            return $this->ResponseStatus(400, 'Failed Validator Error, Please Complete All Data! ' . $ErrorMsg, new \stdClass());
        }
        $movie = MovieMasterModel::with('genre')->with('director')->with('actor')->where('movie_id',$request['movie_id'])->first();
        if (empty($movie)) {
            return $this->ResponseStatus('404', 'FAILED DATA NOT FOUND', new \stdClass());
        }
        $resource = new Item($movie, new MovieTransformerDetail);

        return $this->ResponseStatus(200, 'Movie Detail ', $this->fractal->createData($resource)->toArray()['data']);
    }
    public function destroy(Request $request){
        $PostRequest = $request->only(
            'movie_id'
        );

        $role = [
            'movie_id' => 'Required'
        ];
        $ErrorMsg = $this->Validator($PostRequest, $role);

        if (!empty($ErrorMsg)) {
            return $this->ResponseStatus(400, 'Failed Validator Error, Please Complete All Data! ' . $ErrorMsg, new \stdClass());
        }
        if(!MovieMasterModel::where('movie_id',$request['movie_id'])) return $this->ResponseStatus('404', 'FAILED DATA NOT FOUND', new \stdClass());

        if(MovieMasterModel::where('movie_id',$request['movie_id'])->delete()){
            return $this->ResponseStatus(200, 'Movie Deleted ', new \stdClass());
        }
        return $this->ResponseStatus(400, 'Movie Failed to delete ', new \stdClass());
    }

}
