<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Movies\MovieMasterModel;
use App\Models\Support\GenresModel;
use App\Models\Support\ActorsModel;
use App\Models\Support\DirectorsModel;

class MovieTransformerDetail extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(MovieMasterModel $movie)
    {
	    return [
	        'movie_id'       => (int) $movie->movie_id,
	        'title'          => $movie->title,
	        'year'           =>  $movie->year,
	        'duration'       =>  (int) $movie->duration,
	        'language'       =>  $movie->language,
	        'release_date'   =>  $movie->release_date,
	        'country'        =>  $movie->country,
            'genres'         => $this->genres($movie->genre),
            'actors'         => $this->actors($movie->actor),
            'directors'      => $this->directors($movie->director)
	    ];
    }
    public function genres($data){
        $collect =  collect(GenresModel::get());
            $data= $data->map(function($key) use($collect){
                $key['genre_title']  = $collect->where('genres_id', $key['genres_id'])->pluck('genre_title')->first();
                return $key;
        });
        return $data;
    }
    public function actors($data){
        $collect =  collect(ActorsModel::get());
            $data= $data->map(function($key) use($collect){
                $key['actors_name']  = $collect->where('actors_id', $key['actors_id'])->pluck('actors_name')->first();
                return $key;
        });
        return $data;
    }
    public function directors($data){
        $collect =  collect(DirectorsModel::get());
            $data= $data->map(function($key) use($collect){
                $key['directors_name']  = $collect->where('directors_id', $key['directors_id'])->pluck('director_name')->first();
                return $key;
        });
        return $data;
    }
}
