<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Movies\MovieMasterModel;

class MovieTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(MovieMasterModel $movie)
    {
	    return [
	        'movie_id'             => (int) $movie->movie_id,
	        'title'          => $movie->title,
	        'year'           =>  $movie->year,
	        'duration'       =>  (int) $movie->duration,
	        'language'       =>  $movie->language,
	        'release_date'   =>  $movie->release_date,
	        'country'        =>  $movie->country,
            // 'links'   => [
            //     [
            //         'uri' => 'products/'.$product->id,
            //     ]
            // ],
	    ];
    }
}
