<?php

namespace App\Models\Support;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class DirectorsModel extends Model
{

    use SoftDeletes;

    protected $table   = 'directors';
	public $primarykey = 'directors_id';
    public $timestamps = true;
    protected $fillable = [
		'director_name',
		'gender'
	];
		
	protected $hidden = [
		'updated_at',
		'created_at',
		'deleted_at',
		'status'
	];
	public function movie()
    {
        return $this->hasMany('App\Models\Movie\MovieMasterModel','directors_id', 'directors_id');
    }


}
