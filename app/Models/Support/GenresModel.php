<?php

namespace App\Models\Support;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class GenresModel extends Model
{

    use SoftDeletes;

    protected $table   = 'genres';
	public $primarykey = 'genres_id';
    public $timestamps = true;
    protected $fillable = [
		'genre_title'
	];
	protected $casts = [
		'genre_title' 	=> 'string'
	];
		
	protected $hidden = [
		'updated_at',
		'created_at',
		'deleted_at',
		'status'
	];
	public function movie()
    {
        return $this->hasMany('App\Models\Movie\MovieMasterModel','genres_id', 'genres_id');
    }

}
