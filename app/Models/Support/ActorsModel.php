<?php

namespace App\Models\Support;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class ActorsModel extends Model
{

    use SoftDeletes;

    protected $table   = 'actors';
	public $primarykey = 'actors_id';
    public $timestamps = true;
    protected $fillable = [
		'actors_name',
		'gender'
	];
		
	protected $hidden = [
		'updated_at',
		'created_at',
		'deleted_at',
		'status'
    ];

	public function movie()
    {
        return $this->hasMany('App\Models\Movie\MovieMasterModel','actors_id', 'actors_id');
    }
}
