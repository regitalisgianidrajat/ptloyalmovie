<?php

namespace App\Models\Movies;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MovieGenresModel extends Model
{
    use SoftDeletes;
    protected $table   = 'movie_genres';
	public $primarykey = 'movie_genres_id';
    public $timestamps = true;
    protected $fillable = [
		'movie_id',
		'genres_id'
	];
	protected $casts = [
		'movie_id' 		=> 'integer',
		'genres_id' 	=> 'integer',
	];
		
	protected $hidden = [
		'updated_at',
		'deleted_at',
		'created_at'
	];
    public function genre()
    {
        return $this->belongsTo('App\Models\Support\GenresModel', 'movie_genres_id', 'genres_id');
    }
    public function movie()
    {
        return $this->belongsTo('App\Models\Movies\MovieMasterModel', 'movie_genres_id', 'movie_id');
    }


}
