<?php

namespace App\Models\Movies;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MovieActorsModel extends Model
{
	use SoftDeletes;
    protected $table   = 'movie_actors';
	public $primarykey = 'movie_actors_id';
    public $timestamps = true;
    protected $fillable = [
		'movie_id',
		'actors_id'
	];
	protected $casts = [
		'movie_id' 		=> 'integer',
		'actors_id' 	=> 'integer'
	];
		
	protected $hidden = [
		'updated_at',
		'deleted_at',
		'created_at'
	];
	public function actor()
    {
        return $this->belongsTo('App\Models\Support\ActorsModel', 'movie_actors_id', 'actors_id');
    }
    public function movie()
    {
        return $this->belongsTo('App\Models\Movies\MovieMasterModel', 'movie_actors_id', 'movie_id');
    }

}
