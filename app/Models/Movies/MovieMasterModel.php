<?php

namespace App\Models\Movies;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class MovieMasterModel extends Model
{

    use SoftDeletes;

    protected $table   = 'movies_master';
	public $primarykey = 'movie_id';
    public $timestamps = true;
    protected $fillable = [
        'title',
        'year',
        'duration',
        'language',
        'release_date',
		'country'
    ];
    protected $casts = [
		'title' 		=> 'string',
		'year' 	        => 'integer',
		'duration' 	    => 'string',
		'language' 	    => 'string',
		'country' 	    => 'string',
	];
		
	protected $hidden = [
		'updated_at',
		'created_at',
		'deleted_at',
		'status'
	];
    public function genre()
    {
        return $this->hasMany('App\Models\Movies\MovieGenresModel','movie_id', 'movie_id');
	}
	public function director()
    {
        return $this->hasMany('App\Models\Movies\MovieDirectorsModel','movie_id', 'movie_id');
	}
	public function actor()
    {
        return $this->hasMany('App\Models\Movies\MovieActorsModel','movie_id', 'movie_id');
    }

}
