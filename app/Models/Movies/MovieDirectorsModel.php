<?php

namespace App\Models\Movies;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MovieDirectorsModel extends Model
{

	use SoftDeletes;
	protected $table   = 'movie_directors';
	public $primarykey = 'movie_directors_id';
    public $timestamps = true;
    protected $fillable = [
		'movie_id',
		'directors_id'
	];
	protected $casts = [
		'movie_id' 		=> 'integer',
		'directors_id' 	=> 'integer'
	];	
	protected $hidden = [
		'updated_at',
		'deleted_at',
		'created_at'
	];
	public function director()
    {
        return $this->belongsTo('App\Models\Support\DirectorsModel', 'movie_directors_id', 'directors_id');
    }
    public function movie()
    {
        return $this->belongsTo('App\Models\Movies\MovieMasterModel', 'movie_directors_id', 'movie_id');
    }

}
