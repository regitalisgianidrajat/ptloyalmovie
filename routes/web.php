<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// routes cms
    Route::group(['prefix' => 'movies'], function () {
        Route::get('/', 'CMS\MoviesController@index');
        Route::get('/add', 'CMS\MoviesController@create');
        Route::post('/save', 'CMS\MoviesController@store');
        Route::get('/edit/{id}', 'CMS\MoviesController@edit');
        Route::put('/update/{id}', 'CMS\MoviesController@update');
        Route::get('/delete/{id}', 'CMS\MoviesController@destroy');
    });
// routes api
    //credential
    Route::post('api/get-token','API\CredentialController@create');
    //api
    Route::group(['middleware' => ['checkToken'], 'prefix' => 'api/movies'], function () {
        Route::get('/','API\MoviesController@index');
        Route::get('detail','API\MoviesController@show');
        Route::delete('/','API\MoviesController@destroy');
    });