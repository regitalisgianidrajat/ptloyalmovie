<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Support\ActorsModel;
use Faker\Generator as Faker;

$factory->define(ActorsModel::class, function (Faker $faker) {
    return [
        'actors_name' => $this->faker->name,
        'gender' => $this->faker->randomElement(['M' ,'W'])
    ];
});


