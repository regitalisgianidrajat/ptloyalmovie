<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Support\DirectorsModel;
use Faker\Generator as Faker;

$factory->define(DirectorsModel::class, function  (Faker $faker) {
    return [
        'director_name' => $this->faker->name,
        'gender' => $this->faker->randomElement(['M' ,'W'])
    ];
});
