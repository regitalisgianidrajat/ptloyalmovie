<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Movies\MovieMasterModel;
use Faker\Generator as Faker;

$factory->define(MovieMasterModel::class, function (Faker $faker) {
    return [
        'title' => $this->faker->sentence(3),
        'year' => '2019',
        'duration' => $this->faker->randomNumber,
        'language'=> $this->faker->country,
        'release_date' => $this->faker->dateTime(),
        'country'=> $this->faker->country
    ];
});
