<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Support\GenresModel;
use Faker\Generator as Faker;

$factory->define(GenresModel::class, function (Faker $faker) {
    return [
        'genre_title' => $this->faker->sentence(1)
    ];
});
