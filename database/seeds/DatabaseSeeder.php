<?php

use Illuminate\Database\Seeder;
use App\Models\Support\DirectorsModel;
use App\Models\Support\ActorsModel;
use App\Models\Support\GenresModel;
use App\Models\Movies\MovieMasterModel;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        factory(DirectorsModel::class, 5)->create();
        factory(ActorsModel::class, 5)->create();
        factory(GenresModel::class, 5)->create();
        factory(MovieMasterModel::class, 5)->create();
        $this->call(CredentialSeeder::class);

    }
}
