<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies_master', function (Blueprint $table) {
            $table->bigIncrements('movie_id');
            $table->string('title');
            $table->integer('year');
            $table->string('duration');
            $table->string('language');
            $table->date('release_date');
            $table->string('country');
            $table->dateTime('deleted_at')->nullable();
            $table->enum('status', [0, 1])->comment('0 active | 1 inactive')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies_master');
    }
}
